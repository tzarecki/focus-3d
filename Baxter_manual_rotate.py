import baxter_interface
import numpy
import rospy
import math
import ik_solver
import time
import baxter_external_devices
from baxter_interface import CHECK_VERSION
from geometry_msgs.msg import (    
    Point,
    Quaternion,
)
DEBUG_MODE = False


def init():
    global limbs

    if DEBUG_MODE:
        return
    left = baxter_interface.Limb('left')
    right = baxter_interface.Limb('right')
    limbs = {"left": left, "right": right}


saved_angles = None


def init_rotate_mode(limb_name):
    global saved_angles, limbs
    limb = limbs[limb_name]
    saved_angles = limb.joint_angles()


def rotate(limb_name,rotation_x,rotation_y):
    global limbs, saved_angles
    limb = limbs[limb_name]

    lj = limb.joint_names()
    limb.set_joint_position_speed(0.5)   

    joint_command = {}

    def set_command(limb, joint_name, rel_angle):
        new_ang = saved_angles[joint_name] + rel_angle
        diff = 0

        angs = limb.joint_angles()
        if joint_name in angs:
            cur_ang = limb.joint_angles()[joint_name]
            diff = math.fabs(cur_ang - new_ang)

        if joint_name not in angs or diff > 0.05:
            joint_command[joint_name] = new_ang            
        
    set_command(limb, lj[6], rotation_x*1.5)
    set_command(limb, lj[5], -rotation_y)
    if joint_command:
        limb.set_joint_positions(joint_command)
        print "rotating. Rotation data: ", rotation_x, rotation_y
    return bool(joint_command)