import baxter_interface
from baxter_interface import CHECK_VERSION

DEBUG_MODE = False
grippers ={}

def init():
    global grippers
    if DEBUG_MODE:
        return

    left = baxter_interface.Gripper('left', CHECK_VERSION)
    left.calibrate()
    right = baxter_interface.Gripper('right', CHECK_VERSION)
    right.calibrate()
    grippers = {"left": left, "right": right}


def get_position(gripper_name):
    if gripper_name in grippers:
        return grippers[gripper_name].position()
    return 100


# changes gripper position.
# gripper name can be 'left' or 'right'
# position is float value between 0 and 100%.
# 0% means closed and 100% means open
def set_position(gripper_name, position):
    global grippers
    if DEBUG_MODE:
        print "Set gripper ", gripper_name, " in position ", position
        return
    if gripper_name in grippers:
        gr = grippers[gripper_name]
        if 0 <= position <= 100:
            gr.command_position(position)
