import baxter_interface
import rospy
import ik_solver
import time
from baxter_interface import CHECK_VERSION
from geometry_msgs.msg import (    
    Point,
    Quaternion,
)

DEBUG_MODE = False

face_down = Quaternion(x=0.0, y=1.0, z=0.0, w=0.0)
face_up = Quaternion(x=0.0, y=0.0, z=0.0, w=1.0)
face_left = Quaternion(x=0.5, y=-0.5, z=0.5, w=0.5)
face_forward = Quaternion(x=0.0, y=0.5, z=0.0, w=0.5)
# facegripleftvertical = Quaternion(x=0.0, y=0.5, z=-0.5, w=0.0)
face_right = Quaternion(x=-0.5, y=0.5, z=0.5, w=0.5)
# facegriprightvertical = Quaternion(x=-0.5, y=0.0, z=0.0, w=0.5)


def init(face_down_height_, face_up_height_):
    global cur_orient, limbs, rs, face_down_height, face_up_height
    if DEBUG_MODE:
        return
    rospy.init_node('move_arms')

    face_down_height = face_down_height_
    face_up_height = face_up_height_

    rs = baxter_interface.RobotEnable(CHECK_VERSION)
    rs.enable()

    def clean_shutdown():
        rs.disable()
    rospy.on_shutdown(clean_shutdown)

    left = baxter_interface.Limb('left')
    right = baxter_interface.Limb('right')

    limbs = {"left": left, "right": right}
 
    cur_orient = face_down


# gets current position of the limb (coordinates of the grippers end)
def get_position(limb):
    pose = limb.endpoint_pose()
    while 'position' not in pose:
        pose = limb.endpoint_pose()
    return pose["position"]


# gets current orientation of the limb (quaternion)
def get_cur_orient(limb):
    pose = limb.endpoint_pose()
    while 'orientation' not in pose:
        pose = limb.endpoint_pose()
    return pose["orientation"]


def get_limb(limb_name):
    if limb_name in limbs:
        return limbs[limb_name]
    return limbs["right"]


DEFAULT_THRESHOLD = 0.008726646


def move_arm(limb_name, x, y, z, accuracy=1):
    """
    Move the arm to the goal.
    :param limb_name: "left" or "right"
    :param x, y, z: coordinates of the goal point
    :param accuracy: defines how accurate should be the movement. accuracy = 1 is standard accuracy, and (for example)
        accuracy = 0.2 means 5 times less accurate movement
    :return: coordinates (as Point object) of the point where arm stopped
    """
    global limbs
    if DEBUG_MODE:
        print "moving limb ", limb_name, "to point (", x, ", ", y, ", ", z, ")"
        time.sleep(0.5)
        return Point(x=x, y=y, z=z)
    
    limb = limbs[limb_name]
    limb.set_joint_position_speed(0.4)
    goal = Point(x=x, y=y, z=z)
    orient = get_orient(limb_name, x, y, z)    
    limb_joints = ik_solver.solve(limb_name, get_position(limb), goal, orient)    
    if limb_joints is not None:
        limb.move_to_joint_positions(limb_joints, 15.0, DEFAULT_THRESHOLD / accuracy)

    return get_position(limb)


def get_orient(limb_name, x, y, z):
    if face_down_height < z <= face_up_height:
        if x > 0.7:
            print "forward"
            return face_forward
        else:
            if limb_name is 'right':
                if y > -0.5:
                    return face_right
                else:
                    return face_left

            if limb_name is 'left':
                if y < 0.5:
                    return face_left
                else:
                    return face_right

    if z > face_up_height:
        return face_up
    return face_down


cur_orient = None


def save_orient(limb_name):
    global cur_orient
    cur_orient = get_cur_orient(limbs[limb_name])


def set_position(limb_name, x, y, z, speed):
    global limbs, rs
    if DEBUG_MODE:
        return Point(x=x, y=y, z=z)

    limb = limbs[limb_name]
    limb.set_joint_position_speed(speed)

    cur_pose = get_position(limb)

    #print "-----------------------------------"
    #print "current pose: ", cur_pose
        
    limb_joints = ik_solver.solve(limb_name, cur_pose, Point(x=x, y=y, z=z), cur_orient)
    if limb_joints is not None:
        limb.set_joint_positions(limb_joints)

    return get_position(limb)