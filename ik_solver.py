import argparse
import sys

import rospy
import math
import time
import copy

from geometry_msgs.msg import (
    PoseStamped,
    Pose,
    Point,
    Quaternion,
)
from std_msgs.msg import Header

from baxter_core_msgs.srv import (
    SolvePositionIK,
    SolvePositionIKRequest,
)


def send_request(limb, pos, orient, seed_angles=None):
    ns = "ExternalTools/" + limb + "/PositionKinematicsNode/IKService"
    iksvc = rospy.ServiceProxy(ns, SolvePositionIK)
    ikreq = SolvePositionIKRequest()
    hdr = Header(stamp=rospy.Time.now(), frame_id='base')
    poses = {
        str(limb): PoseStamped(header=hdr,
                               pose=Pose(position=pos, orientation=orient))}

    ikreq.pose_stamp.append(poses[limb])
    if seed_angles is not None:
        ikreq.seed_angles.append(seed_angles)

    try:
        rospy.wait_for_service(ns, 5.0)
        resp = iksvc(ikreq)
    except (rospy.ServiceException, rospy.ROSException), e:
        rospy.logerr("Service call failed: %s" % (e,))
        return None

    if resp.isValid[0]:
        print("SUCCESS - Valid Joint Solution Found:")
        return resp.joints[0]
    else:
        print pos
        print("INVALID POSE - No Valid Joint Solution Found.")

    return None


def distance(point1, point2):
    return math.sqrt((point1.x - point2.x) ** 2
                     + (point1.y - point2.y) ** 2
                     + (point1.z - point2.z) ** 2)


def solve(limb_name, init_pos, init_goal, orient):
    pos = copy.deepcopy(init_pos)
    attempt = 1
    cur_angles = None
    angles = None

    # while goal is not reached, move toward the goal
    while attempt < 10 and distance(pos, init_goal) > 0.005:
        """It will do attempt to move arm to the goal point.
        If the goal point is unreachable in one movement, it will try to move arm only half way to the goal,
        then 1/4, 1/8 etc. of the original vector form the current position to the goal."""

        goal = copy.deepcopy(init_goal)
        for i in range(0, 6):
            #print 'cur position: ', pos
            #print 'cur goal: ', goal
            angles = send_request(limb_name, goal, orient, cur_angles)
            
            if angles is None:
                goal.x = (goal.x + pos.x) / 2
                goal.y = (goal.y + pos.y) / 2
                goal.z = (goal.z + pos.z) / 2
            else:
                pos = copy.deepcopy(goal)
                cur_angles = copy.deepcopy(angles)
                break

        if angles is None:
            break
                
        print 'attempt', attempt
        attempt += 1

    if cur_angles is None:
        return None

    # Format solution into Limb API-compatible dictionary
    return dict(zip(cur_angles.name, cur_angles.position))