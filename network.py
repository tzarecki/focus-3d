import SocketServer
import threading
import copy
import sys
import signal


# Create the server, binding to HOST on PORT
def init(host, port, table_height_):
    global table_height, server
    print 'Starting'
    # Create the server, binding to HOST on PORT
    server = BaxterTCPServer((host, port))

    def signal_handler(signal, frame):
            print('You pressed Ctrl+C!')
            server.stop()
            sys.exit(0)
    signal.signal(signal.SIGINT, signal_handler)
    table_height = table_height_


def get_data():
    return server.get_data()


class BaxterTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    lock = threading.Lock()
    data = None

    def __init__(self, server_address):
        """Constructor.  May be extended, do not override."""
        SocketServer.TCPServer.allow_reuse_address = True
        SocketServer.TCPServer.__init__(self, server_address, BaxterTCPHandler, bind_and_activate=True)
        self.handler = None  
        # Start a thread with the server -- that thread will then start one
        # more thread for each request
        self.server_thread = threading.Thread(target=self.serve_forever)
        # Exit the server thread when the main thread terminates
        self.server_thread.daemon = True
        self.server_thread.start()     

    def finish_request(self, request, client_address):
        """Finish one request by instantiating RequestHandlerClass."""
        handler = self.RequestHandlerClass(request, client_address, self)
        if handler.data is not None:
            self.lock.acquire()
            self.data = handler.data
            self.lock.release()    

    def get_data(self):
        self.lock.acquire()
        data = copy.deepcopy(self.data)
        self.lock.release()
        if (data is None) or ('mode' not in data):
            data = {'mode': 0}

        if 'x' in data and 'z' in data:
            data['z'] -= 0.02
            if data['z'] < table_height + 0.01:
                data['z'] = table_height + 0.01
        if 'grab_factor' in data:
            data['grab_factor'] = (1 - data['grab_factor']) * 100
        return data

    def stop(self):        
        self.shutdown()


class BaxterTCPHandler(SocketServer.StreamRequestHandler):
    """
    The RequestHandler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    data = None

    def handle(self):
        # self.rfile is a file-like object created by the handler;
        # we can now use e.g. readline() instead of raw recv() calls

        try:
            self.data = {'mode': int(self.rfile.readline().strip()),
                         'x': float(self.rfile.readline().strip()),
                         'y': float(self.rfile.readline().strip()),
                         'z': float(self.rfile.readline().strip()),
                         # the up/down gesture speed
                         'gesture_x': float(self.rfile.readline().strip()),
                         # the left/right gesture speed
                         'gesture_y': float(self.rfile.readline().strip()),
                         # the forward/backward gesture speed
                         'gesture_z': float(self.rfile.readline().strip()),
                         'rotation_x': float(self.rfile.readline().strip()),
                         'rotation_y': float(self.rfile.readline().strip()),
                         'manual_mode': self.rfile.readline().strip(),
                         'grab_factor': float(self.rfile.readline().strip())}
            
        except: #data is not correct
            self.data = None
            print "Recieved incorrect package"


