import socket
import sys

HOST, PORT = "192.168.0.107", 5020
#HOST, PORT = "192.168.0.221", 5007
#HOST, PORT = "134.96.63.204", 5007

# Create a socket (SOCK_STREAM means a TCP socket)
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

x=0.0
y=0.0
z=0.0
gesture_x = 0.0
gesture_y = 0.0
gesture_z = 0.0
rotation_x = 0.0
rotation_y = 0.0
manual_mode = "move"

try:
    # Connect to server and send data
    sock.connect((HOST, PORT))
    mode = sys.argv[1]

    if len(sys.argv) >= 6:
        rotation_x = sys.argv[5]

    if len(sys.argv) >= 7:
        rotation_y = sys.argv[6]

    if len(sys.argv) >= 8:
        manual_mode = sys.argv[7]

    if mode == '1':
        x = sys.argv[2]
        y = sys.argv[3]
        z = sys.argv[4]
    else:
        gesture_x = sys.argv[2]
        gesture_y = sys.argv[3]
        gesture_z = sys.argv[4]
    data = "{0}\n{1}\n{2}\n{3}\n{4}\n{5}\n{6}\n{7}\n{8}\n{9}\n0\n".format(mode, x, y, z, gesture_x, gesture_y, gesture_z, rotation_x, rotation_y, manual_mode)
    sock.sendall(data)

finally:
    sock.close()

print "Sent:     {}".format(data)
