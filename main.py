import network
import Baxter_manual_rotate as manual_rotate
import Baxter_arm_move as move_arm
import grippers
import math
import copy
from geometry_msgs.msg import Point

HOST = '192.168.0.107'
#HOST = '134.96.63.205'
PORT = 5020  # Arbitrary non-privileged port
TABLE_HEIGHT = -0.145
FACE_DOWN_HEIGHT = 0.30  # if z < FACE_DOWN_HEIGHT then orientation is facedown
FACE_UP_HEIGHT = 0.55  # if z >= FACE_UP_HEIGHT then orientation is faceup


default_positions = {"right": Point(x=0.457579481614, y=-0.551981417433, z=0.0388352386502),
                     "left": Point(x=0.457579481614, y=0.551981417433, z=0.0388352386502)}

cur_coords = {"right": Point(x=0.0, y=0.0, z=0.0),
              "left": Point(x=0.0, y=0.0, z=0.0)}
cur_grip_pos = {"right": 0, "left": 0}
cur_limb = "right"


def reset_limb(limb):
    global cur_limb, default_positions
    pos = default_positions[limb]
    cur_limb = limb
    set_gripper(100)
    move_to_coordinates(pos)


def choose_limb(coords):
    # we will not change arm if the new goal is in the middle ot the table (30cm area)
    if math.fabs(coords.y) < 0.15:
        return cur_limb

    # do not change limb if the gripper is not open
    if grippers.get_position(cur_limb) < 95:
        return cur_limb

    elif coords.y > 0:
        return "left"
    else:
        return "right"


def distance(point1, point2):
    return math.sqrt((point1.x - point2.x) ** 2
                     + (point1.y - point2.y) ** 2
                     + (point1.z - point2.z) ** 2)


BUFFER_DISTANCE = 0.15


def move_to_coordinates(goal_):
    global cur_coords, cur_limb

    goal = copy.deepcopy(goal_)
    # calculate distance between current point and the new one.
    cur_limb = choose_limb(goal)
    cur_coord = copy.deepcopy(cur_coords[cur_limb])

    # if distance is > 2 cm we move arm (avoiding jittering)
    if distance(cur_coord, goal) > 0.02:

        # lift arm up before the movement if it is too close to the table surface
        if cur_coord.z <= TABLE_HEIGHT + BUFFER_DISTANCE:
            cur_coords[cur_limb] = move_arm.move_arm(cur_limb, cur_coord.x, cur_coord.y, cur_coord.z + BUFFER_DISTANCE, 0.2)

        # prevent doing rotation from face_down to face_up orientation (or vise versa) in one step
        # because it does big movement and sometimes hits the table
        if (cur_coord.z < FACE_DOWN_HEIGHT and goal.z >= FACE_UP_HEIGHT)\
                or (cur_coord.z > FACE_DOWN_HEIGHT and goal.z <= FACE_UP_HEIGHT):
            cur_coords[cur_limb] = move_arm.move_arm(cur_limb, goal.x, goal.y, (FACE_DOWN_HEIGHT + FACE_UP_HEIGHT) / 2, 0.2)

        changed_goal_z = goal.z
        if goal.z <= TABLE_HEIGHT + BUFFER_DISTANCE:
            changed_goal_z = TABLE_HEIGHT + BUFFER_DISTANCE

        cur_coords[cur_limb] = move_arm.move_arm(cur_limb, goal.x, goal.y, changed_goal_z, 1)

        # pull down the arm if the goal is on the table
        if changed_goal_z != goal.z:
            # if the goal point was changed during the time we spent on movements, do not pull down the arm
            new_data = network.get_data()
            if distance(goal, Point(x=new_data['x'], y=new_data['y'], z=new_data['z'])) < 0.05:
                cur_coords[cur_limb] = move_arm.move_arm(cur_limb, goal.x, goal.y, goal.z, 1)


MAX_SPEED = 0.5
prev_manual_mode = None


def handle_gestures_relative(data):
    global prev_manual_mode
    cur = cur_coords[cur_limb]

    # print "Mode 2, gesture data: ", (data['gesture_x'], data['gesture_y'], data['gesture_z'])

    goal = Point(x=cur.x + data['gesture_x'], y=cur.y + data['gesture_y'], z=cur.z + data['gesture_z'])

    if goal.z < TABLE_HEIGHT + 0.01:
        goal.z = TABLE_HEIGHT + 0.01

    dist = distance(move_arm.get_position(move_arm.get_limb(cur_limb)), goal)
    speed = min(MAX_SPEED, dist*3)

    if data['manual_mode'] == "rotate":
        if prev_manual_mode != "rotate":
            manual_rotate.init_rotate_mode(cur_limb)
        manual_rotate.rotate(cur_limb, data['rotation_x'], data['rotation_y'])
    else:
        if prev_manual_mode != "move":
            move_arm.save_orient(cur_limb)
        # if distance(move_arm.get_position(move_arm.get_limb(cur_limb)), goal) > 0.01:
        move_arm.set_position(cur_limb, goal.x, goal.y, goal.z, speed)
    prev_manual_mode = data['manual_mode']


def set_gripper(position):
    grip_pos = cur_grip_pos[cur_limb]
    
    # if difference is more than 1% - change the position
    if math.fabs(grip_pos - position) > 1:
        grippers.set_position(cur_limb, position)
        cur_grip_pos[cur_limb] = position

# Create the TCP server
network.init(HOST, PORT, TABLE_HEIGHT)

move_arm.init(FACE_DOWN_HEIGHT, FACE_UP_HEIGHT)

reset_limb("right")
reset_limb("left")

grippers.init()
manual_rotate.init()

print("robot initiaze")

print "starting main loop"
while True:
    # receive command from Unity server
    data = network.get_data()

    if data['mode'] != 2:
        prev_manual_mode = None

    if data['mode'] == 1:
        print "Mode 1, received coordinates ", (data['x'], data['y'], data['z'])
        set_gripper(data['grab_factor'])
        move_to_coordinates(Point(x=data['x'], y=data['y'], z=data['z']))
        set_gripper(data['grab_factor'])

    elif data['mode'] == 2:
        print data
        set_gripper(data['grab_factor'])
        handle_gestures_relative(data)
        set_gripper(data['grab_factor'])

    else:
        print "Mode 0"
        reset_limb("right")
        reset_limb("left")